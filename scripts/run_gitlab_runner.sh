#!/bin/bash
# run_gitlab_runner.sh

GITLAB_RUNNER_BIN=~/bin/gitlab-runner
GITLAB_RUNNER_DIR=$HOME/gitlab-runner
GITLAB_LOGS_DIR=$HOME/logs

GITLAB_RUNNER_CMD=("${GITLAB_RUNNER_BIN}" run "--working-directory=${GITLAB_RUNNER_DIR}")

SCRIPTS_DIR=$HOME/scripts

MODIFY_STATUS=1

# source common functions
. "${SCRIPTS_DIR}/run_service.sh"

start_service() {
	"${GITLAB_RUNNER_CMD[@]}" > ${GITLAB_LOGS_DIR}/gitlab-runner-$(date '+%F_%T').log 2>&1 &
}

stop_service() {
  local process_cmdline=$1
  local kill_signal=$2
  local kill_sig_arg=
  if [ -n "$kill_signal" ]; then
    kill_sig_arg="-${kill_signal}"
  fi
  local pid=$(get_pid "${process_cmdline}")
  if [ -n "${pid}" ]; then
    kill ${kill_sig_arg} "${pid}"
    echo "Stopped: ${pid}"
    return 0
  else
    echo "Not running"
    return 1
  fi
}

### cmd line args
case "$1" in
  start)
    MODIFY_STATUS=0
    ;;
  stop)
    stop_service "${GITLAB_RUNNER_CMD[*]}"
    exit $?
    ;;
esac

status=$(get_status "${GITLAB_RUNNER_CMD[*]}")

if [ ! "${status}" -eq 0 ]; then
	if [ "$MODIFY_STATUS" -eq 0 ]; then
		echo "starting gitlab runner"
		start_service
		status=$(get_status "${GITLAB_RUNNER_CMD[*]}")
	else
		echo "gitlab runner not running"
	fi
else
	pid=$(get_pid "${GITLAB_RUNNER_CMD[*]}")
	echo "gitlab runner running! pid: $pid"
fi

exit $status
