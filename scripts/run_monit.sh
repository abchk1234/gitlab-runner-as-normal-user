#!/bin/bash
# check_monit.sh: to check if monit is running and start it if not

MONIT_BIN=~/bin/monit
MONIT_CONF=~/.monitrc

if ! $MONIT_BIN -c $MONIT_CONF status > /dev/null; then
#if ! /usr/bin/pgrep "monit" > /dev/null; then
	echo "[$(date +%F_%T)] starting monit"
	$MONIT_BIN -c $MONIT_CONF
else
	echo "[$(date +%F_%T)] monit aleady running!"
fi

exit $?
