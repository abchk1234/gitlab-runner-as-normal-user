#!/bin/bash
# run_service.sh: To check and run (start/restart/status) certain service(s)

### FUNCTIONS
# To check if service is running
get_status() {
  local process_cmdline=$1
  local status=
  pgrep -f "${process_cmdline}" > /dev/null 2>&1
  echo "$?"
}

# To get the pid of the script if running
get_pid() {
  local process_cmdline=$1  
  local pid=
  pid=$(pgrep -f "${process_cmdline}")
  echo "${pid}"
}

print_status() {
  local process_cmdline=$1
  local status
  status=$(get_status "${process_cmdline}")
  if [ $status -eq 0 ]; then
    local pid
    pid=$(get_pid "${process_cmdline}")
    echo -e "Running, pid: ${pid}"
  else
    echo "Not running"
  fi
  return "${status}"
}
