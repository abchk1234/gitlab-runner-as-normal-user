To run [gitlab-runner](https://docs.gitlab.com/runner/) as normal, non-root user.

### Commands

~~~~
mkdir ~/bin  ~/gitlab-runner  ~/monit  ~/scripts  ~/logs

cd ~/bin
wget -O gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/v10.8.0/binaries/gitlab-runner-linux-amd64
chmod +x gitlab-runner

cd ~/tmp
wget https://mmonit.com/monit/dist/binary/5.25.2/monit-5.25.2-linux-x64.tar.gz
tar xvf monit-5.25.2-linux-x64.tar.gz
cp monit-5.25.2/bin/monit ~/bin/

cd
~~~~

### Related post

https://abchk1234.wordpress.com/2017/08/18/using-gitlab-ci-for-deployments/#deployment-using-gitlab-runner-as-normal-user
